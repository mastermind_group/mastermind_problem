import requests as rq
import json

mm_url = "https://we6.talentsprint.com/wordle/game/"
register_url = mm_url + "register"
register_dict = {"mode" : "mastermind", "name" : "group1"}
session = rq.Session()
register_with = json.dumps(register_dict)
r = session.post(register_url , json = register_dict)

my_id = r.json()['id']
create_url = mm_url + "create"
create_dict = { "id" : my_id , "overwrite" : True }
create_with = json.dumps(create_dict)
rc = session.post(create_url , json = create_dict)

guess_dict = { "guess" : "string" , "id" : my_id}
guess_url = mm_url + "guess"
guess_with = json.dumps(guess_dict)
rcg = session.post(guess_url , json = guess_dict)





