import random
from itertools import permutations as npr

import requests as rq
import json

class Mastermind:
    def __init__(self, filename):
        self.words_list = self.read_words_from_file(filename)
        self.guess = ""
        self.guess_chars = []
        self.feedback = 0
        self.current_options = self.words_list.copy()
        self.session = rq.Session()
        self.mm_url = "https://we6.talentsprint.com/wordle/game/"
        self.my_id = self.register()

    def read_words_from_file(self, filename):
        with open(filename, 'r') as file:
            return [line.strip() for line in file.readlines()]

    def register(self):
        register_url = self.mm_url + "register"
        register_dict = {"mode": "mastermind", "name": "group1"}
        response = self.session.post(register_url, json=register_dict)
        return response.json()['id']

    def create_game(self):
        create_url = self.mm_url + "create"
        create_dict = {"id": self.my_id, "overwrite": True}
        self.session.post(create_url, json=create_dict)

    def make_guess(self):
        guess_url = self.mm_url + "guess"
        guess_dict = {"guess": self.guess, "id": self.my_id}
        response = self.session.post(guess_url, json=guess_dict)
        return response.json()

    def filter_words(self):
        self.current_options = [word for word in self.current_options if all(any(ch in word for ch in comb) for comb in self.guess_chars)]

    def feedback_0(self):
        self.guess_chars = [ch for ch in self.guess]
        self.current_options = [word for word in self.current_options if all(x not in word for x in self.guess_chars)]

    def feedback_1to4(self):
        self.guess_chars = [''.join(letter_tuple) for letter_tuple in set(npr(self.guess, self.feedback))]
        self.filter_words()

    def feedback_5(self):
        for word in self.current_options:
            if all(ch in word for ch in self.guess):
                return word

    def process_feedback(self, feedback):
        if feedback == 0:
            self.feedback_0()
        elif 1 <= feedback <= 4:
            self.feedback = feedback
            self.feedback_1to4()
        elif feedback == 5:
            return self.feedback_5()

    def start_game(self):
        self.create_game()
        while True:
            self.guess = random.choice(self.current_options)
            response = self.make_guess()
            feedback_received = response["feedback"]
            result = self.process_feedback(feedback_received)
            if feedback_received == 5:
                print(f"Bot guessed the correct word: {result}")
                break
        return result

M = Mastermind("5words.txt")
print(M.start_game())