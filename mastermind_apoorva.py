# ---------------------
# Mastermind-Solver Bot
# ---------------------

import random
from itertools import permutations as npr

import requests as rq
import json


class Mastermind:
    WORDS_LIST = []
    def __init__(self, filename):
        f = open(filename)
        Mastermind.WORDS_LIST = [word.strip() for word in f.readlines()]
        self.guess = ""#random.choice(Mastermind.WORDS_LIST)
        self.guess_chars = []
        self.feedback = ""
        self.current_options = Mastermind.WORDS_LIST
    
    def __str__(self):
        return Mastermind.WORDS_LIST[0]
    
    def filter_words(self):
        list_options = []
        for word in self.current_options:
            for comb in self.guess_chars:
                if all(letter in word for letter in comb):
                    list_options.append(word)
                    break
        self.current_options = list_options

    def feedback_0(self):
        self.guess_chars = [ch for ch in self.guess]
        self.current_options = [word for word in self.current_options if all(x not in word for x in self.guess_chars)]

    def feedback_1to4(self):
        self.guess_chars = [''.join(_ for _ in letter_tuple) for letter_tuple in set(npr(self.guess, self.feedback))]
        self.filter_words()

    def feedback_5(self):
        self.guess_chars = set(npr(self.guess, 5))
        for letter_tuple in self.guess_chars:
            word = ''.join(_ for _ in letter_tuple)
            if word in self.current_options:
                return word

    def start_game(self):
        mm_url = "https://we6.talentsprint.com/wordle/game/"
        register_url = mm_url + "register"
        register_dict = {"mode" : "mastermind", "name" : "group1"}
        session = rq.Session()
        register_with = json.dumps(register_dict)
        r = session.post(register_url , json = register_dict)

        my_id = r.json()['id']
        create_url = mm_url + "create"
        create_dict = { "id" : my_id , "overwrite" : True }
        create_with = json.dumps(create_dict)
        rc = session.post(create_url , json = create_dict)

        while self.feedback != 5:
            self.guess = random.choice(self.current_options)
            guess_dict = { "guess" : self.guess , "id" : my_id}
            guess_url = mm_url + "guess"
            guess_with = json.dumps(guess_dict)
            rcg = session.post(guess_url , json = guess_dict)

            feedback = rcg.json().get("feedback")
            self.feedback = feedback

            print(f"Guessed: {self.guess}, Feedback: {self.feedback}")

            if self.feedback == 0:
                self.feedback_0()
            elif 1 <= self.feedback <= 4 :
                self.feedback_1to4()

        self.guess = self.feedback_5()
        guess_dict = { "guess" : self.guess , "id" : my_id}
        guess_url = mm_url + "guess"
        guess_with = json.dumps(guess_dict)
        rcg = session.post(guess_url , json = guess_dict)

        feedback = rcg.json().get("feedback")
        self.feedback = feedback

        print(f"Guessed: {self.guess}, Feedback: {self.feedback}")


M = Mastermind("5words.txt")
M.start_game()

